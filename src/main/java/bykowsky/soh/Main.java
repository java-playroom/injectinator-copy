package bykowsky.soh;

import bykowsky.soh.config.Config;
import bykowsky.soh.demo.ServiceExample;

public class Main {
    public static void main(String[] args) {

        Config config = Config.getInstance();
        ServiceExample myService = config.getServiceExample();
        System.out.println(myService.useGetRepoMethod());
    }
}
