package bykowsky.soh.config;

import bykowsky.soh.annotation.Inject;
import bykowsky.soh.demo.RepositoryExample;
import bykowsky.soh.demo.ServiceExample;

import java.lang.reflect.Field;

public class Config {

    private Config() {
    }

    public ServiceExample getServiceExample() {
        RepositoryExample repo = new RepositoryExample();

        ServiceExample myServiceInstance = new ServiceExample();
        Field[] fields = myServiceInstance.getClass().getDeclaredFields();

        for (Field field : fields) {
            if (field.isAnnotationPresent(Inject.class)) {
                field.setAccessible(true);
                try {
                    field.set(myServiceInstance, repo);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return myServiceInstance;
    }

    public static Config getInstance() {
        return new Config();
    }
}
