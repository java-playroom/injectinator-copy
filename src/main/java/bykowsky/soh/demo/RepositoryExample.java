package bykowsky.soh.demo;

public class RepositoryExample {

    public String get() {
        return "Data from repo";
    }

    public String put(String data) {
        System.out.println("Repo saving data: " + data);
        return "0";
    }
}
