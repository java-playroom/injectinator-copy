package bykowsky.soh.demo;

import bykowsky.soh.annotation.Inject;

public class ServiceExample {

    @Inject
    private final RepositoryExample repo = null;

    public ServiceExample() {
    }

    public String useGetRepoMethod() {
        String repoData = repo.get();
        return repoData + " added service logic";
    }

    public void usePutRepoMethod(String originalData) {
        repo.put(originalData + "additional service stuff");
    }
}
